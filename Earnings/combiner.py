import time
import string
import datetime
import pickle
import re

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def processETime( eT ):
    if eT == 'Time Not Supplied': return 'notSupplied'
    elif eT == 'After Market Close': return 'afterMarket'
    elif eT == 'Before Market Open': return 'beforeMarket'
    elif eT == None: return None
    else: # here we assume it is a time xx:xx pm/am ET/?
        # first find the position of ':'
        hourMin = re.findall(r'[0-9]*:[0-9]*',eT)[0]
        hourMinSplit = hourMin.split(':')
        hour = float(hourMinSplit[0]) + float(hourMinSplit[0])/60. 
        if 'pm' in eT: hour += 12 #i.e. 16.5 means 4:30 pm
        # we assume all times are in EST
        if hour<=9.5: return  'beforeMarket'
        elif hour>=16: return 'afterMarket'
        elif hour>9.5 and hour<16: return 'duringMarket'
        else: return None

def symbCombiner( dictSymb1 , dictSymb2 ):
    '''
    This function gets two dicts (presumably correspond to the same symbols),
    and combine their information.
    Note: we assume dict1 has information from Nasdaq. i.e.:
    {'date':( name, mCap, fiscal, eEPS, nE, EPS)}
    and dict2 has information of Yahoo:
    {'date':( name, eTime )}
    The output is a dict:
    {'date': ( nameNasdaq, nameYahoo, Cap, fiscal, eEPS, nE, EPS, eTime)}
    '''
    output = {}
    if ( dictSymb1 is None ) or ( dictSymb2 is None ): return None
    for date in dictSymb1.keys():
        if date in dictSymb2.keys():
            # process the eTime:
            processedETime = processETime( dictSymb2[date][1] )
            output[date] = ( dictSymb1[date][0], dictSymb2[date][0], dictSymb1[date][1], dictSymb1[date][2], dictSymb1[date][3], dictSymb1[date][4], dictSymb1[date][5], processedETime )
    return output

def combiner( fileName1, fileName2 ):
    '''
    This functions gets the address to two files: Nasdaq/Yahoo
    Each should contains dict of dicts: symbol:date:info
    For symbol in the interseciton of two files we will call symbCombiner
    '''
    dictNasdaq = pickle.load(open(fileName1,'rb'))
    dictYahoo = pickle.load(open(fileName2,'rb'))

    outputTot = {}
    for symb in dictNasdaq.keys(): #jahate khana e
        # in future: some symbols need to be modified before comparison. e.g., ABC/D -> ABC-D
        if symb in dictYahoo.keys():
            outputTot[symb] = symbCombiner( dictNasdaq[symb] , dictYahoo[symb] )

    return outputTot
            
if __name__ == '__main__':
    fromDate = [ (2009,1,1), (2010,1,1),(2011,1,1), (2012,1,1), (2013,1,1), (2014,1,1) ]
    days = [ 365, 365, 365, 366, 365, 304 ]
    startDate = []
    endDate = []
    for i in range(len(days)):
        startDate.append(datetime.date(fromDate[i][0], fromDate[i][1], fromDate[i][2]))
        endDate.append(startDate[i] + datetime.timedelta( days[i]-1 ))

    fileNameNasdaq = "Symb_Date_Info_Nasdaq" + '/' +"EarningData_" + str(startDate[0]) + "_" + str(endDate[-1]) + ".stock"
    fileNameYahoo = "Symb_Date_Info_Yahoo" + '/' +"EarningData_" + str(startDate[0]) + "_" + str(endDate[-1]) + ".stock"

    aggregate = combiner(  fileNameNasdaq, fileNameYahoo )
    fileNameOutput = "Symb_Date_Info" + '/' +"EarningData_" + str(startDate[0]) + "_" + str(endDate[-1]) + ".stock"
    
    pickle.dump(aggregate, open(fileNameOutput,'wb'))

    print aggregate
    print len(aggregate)

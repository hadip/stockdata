import urllib2
from urllib2 import urlopen
import re
import cookielib
from cookielib import CookieJar
import time
import string
import datetime
import pickle

cj = CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
opener.addheaders = [('User-agent','Mozilla/5.0')]


# Alphabetic months
alphMonth = {1:'Jan', 2:'Feb', 3:'Mar', 4:'Apr', 5:'May', 6:'Jun', 7:'Jul', 8:'Aug', 9:'Sep', 10:'Oct', 11:'Nov', 12:'Dec'}
alphMonthRev = { v:k for k,v in alphMonth.items() }

# A funciton that checks if the string is a valid floating point number
def is_number(s):
    '''
    if re.match("^\d+?\.\d+?$", s) is None:
        return False
    else:
        return True
    '''
    try:
        float(s)
        return True
    except ValueError:
        return False

# decode characteristics of a company
def decodeFeatures( name, mCap, fiscal, eEPS, nE, EPS):
    '''
    This function converts the string corresponding to the features to numerical values
    '''

    # processing name
    if len(name) == 0: name_value = None
    else: name_value = name

    # processing market cap
    '''
    We assume that the market cap is either n/a or the last character is M/B
    '''
    if len(mCap) == 0: mCap_value = None
    elif mCap[-1] == 'M':
        if is_number(mCap[:-1]):
            mCap_value = float(mCap[:-1])*1e6 
        else:
            mCap_value = None
    elif mCap[-1] == 'B': 
        if is_number(mCap[:-1]):
            mCap_value = float(mCap[:-1])*1e9
        else: mCap_value = None
    else: mCap_value = None
    

    # processing fiscal
    '''
    The output value for the fiscal will be a tuple (y,m),
    where both m and y are stored as integers.
    For none valid cases, as usual, we return None
    '''
    if len(fiscal) !=7: fiscal_value = None
    elif fiscal[:3] in alphMonthRev.keys():
        month = alphMonthRev[fiscal[:3]]        
        if unicode(fiscal[3:]).isnumeric():
            year = int(fiscal[3:])
            fiscal_value = ( year, month )
        else:
            fiscal_value = None
    else: fiscal_value = None

    # processing eEPS
    if is_number(eEPS):
        eEPS_value = float(eEPS)
    else: 
        eEPS_value = None
        
    # processing nE
    if unicode(nE).isnumeric():
        nE_value = int(nE)
    else:
        nE_value = None

    # processing EPS
    if is_number(EPS):
        EPS_value = float(EPS)
    else: 
        EPS_value = None

    return ( name_value, mCap_value, fiscal_value, eEPS_value, nE_value, EPS_value)
    
        
    


def getEarning(date):
    '''
    The input of the function is a date in (y,m,d) in integeres
    The output is a dict, with keys = symbols, and values are a tuple of its characteristics listed as follows:
    (company name, market cap, fiscal, estimated EPS, number of estimation, EPS)
    If any of the above values could not be extracted, we will put None in the tuple.
    If for the given date the function was not able to extract the data it returns None.
    '''
    # The earning date webpage from Nasdaq
    pageBase = 'http://www.nasdaq.com/earnings/earnings-calendar.aspx?date='
    try:
        # Assume the input is a tuple in intergers (year, month, day)
        year = date[0]
        month = date[1]
        day = date[2]
        # Creatung the date string from the tuple data
        dateString = str(year) + '-' + alphMonth[month] + '-' + str(day) 
        # Full link to the query date webpage
        page = pageBase+dateString
        # Read the source file of the query page
        sourceCode = opener.open(page).read()

        ###############################################################################
        ########################### Earning dates scrapping ###########################
        ###############################################################################
        
        # Find all data for the companies for the given date

        content = re.findall(r'<td[^@]*?/td>',sourceCode)
        # we assume that a content which has a comany name also has the following substring:
        # href="http://www.nasdaq.com/earnings/report/
        keySubString = 'href="http://www.nasdaq.com/earnings/report/'
        # Index of contents with a company name
        compIndex = [ i for i in range(len(content)) if string.find(content[i],keySubString) is not -1]
        # The output of the function
        outputDict ={}
        # Extract information for each company
        for i in compIndex:
            # company name
            compName = re.findall(r'>.*<br',content[i])[0][1:-4]
            # symbol
            symbol = re.findall(r'[(].*?[)]',compName)[-1][1:-1]
            # market cap
            marketCap = re.findall(r'Cap.*</b>',content[i])[0][5:-4]
            if marketCap[0]=='$': marketCap=marketCap[1:]
            # fiscal
            fiscal = ''.join(re.findall(r'\w',content[i+2][4:-5]))
            # estimated EPS
            estimatedEPS = re.findall(r'\$.*',content[i+3])[0][1:-1]
            # number of estimation
            numEstimates = ''.join(re.findall(r'\w',content[i+4][4:-5]))            
            # EPS
            EPS = re.findall(r'\$.*',content[i+6])[0][1:-1]
            
            '''
            print compName
            print symbol
            print marketCap
            print fiscal
            print estimatedEPS
            print numEstimates
            print EPS
            '''

            # if the symbol is not valid goto the next company
            if len(symbol) == 0: continue
            
            else:
                outputDict[symbol] = decodeFeatures( compName, marketCap, fiscal, estimatedEPS, numEstimates, EPS)

        return outputDict
            
    except Exception, e:
        print str(e)
        return None

def get_historical_data( fromDate, days ):
    startDate = datetime.date(fromDate[0], fromDate[1], fromDate[2])
    dataBase = {}
    for i in range(days):
        date = startDate + datetime.timedelta(i)
        dateTuple = (date.year, date.month, date.day)
        dataBase[dateTuple] =  getEarning( dateTuple )
        print str(date) + " completed."
    folderName = "Date_Symb_Info_Nasdaq"
    fileName = folderName + '/' + "EarningData_" + str(startDate) + "_" + str(date) + ".stock"
    pickle.dump(dataBase, open(fileName,'wb'))

if __name__ == '__main__':
    # print getEarning( (2013,6,4))
    fromDate = (2008,1,1)
    days = 366
    get_historical_data( fromDate , days )
    startDate = datetime.date(fromDate[0], fromDate[1], fromDate[2])
    date = startDate + datetime.timedelta( days-1 )
    folderName = "Date_Symb_Info_Nasdaq"
    fileName = folderName + '/' +"EarningData_" + str(startDate) + "_" + str(date) + ".stock"
    res = pickle.load( open( fileName, 'rb') )
    for key in res.keys():
        print ' for the date = ' + str(key)
        print res[key]
        print ' --------------------------------'


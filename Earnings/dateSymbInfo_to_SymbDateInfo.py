import time
import string
import datetime
import pickle

def converter(dateSymbInfo_filename_list):
    '''
    This function gets two filenames: read the data from the file that refers to dict of dict ( date: symb: info ),
    and then converts it to a dict of dict ( symb: date: info ) and returns.
    '''
    dateSymbInfo = {}
    for i in range(len(dateSymbInfo_filename_list)):
        dateSymbInfo.update( pickle.load( open( dateSymbInfo_filename_list[i], 'rb') ) )
    symbDateInfo = {}
    for date in dateSymbInfo.keys():
        if dateSymbInfo[date] == None : continue
        for symb in dateSymbInfo[date].keys():
            if symb not in symbDateInfo.keys():
                symbDateInfo[symb] = {}
            symbDateInfo[symb][date] = dateSymbInfo[date][symb]
    return symbDateInfo
            

if __name__ == '__main__':
    fromDate = [ (2009,1,1), (2010,1,1),(2011,1,1), (2012,1,1), (2013,1,1), (2014,1,1) ]
    days = [ 365, 365, 365, 366, 365, 304 ]
#    folderName = "Date_Symb_Info_Nasdaq"
    folderName = "Date_Symb_Info_Yahoo"
    startDate = []
    endDate = []
    filename = []
    for i in range(len(days)):
        startDate.append(datetime.date(fromDate[i][0], fromDate[i][1], fromDate[i][2]))
        endDate.append(startDate[i] + datetime.timedelta( days[i]-1 ))
        filename.append(folderName + '/' +"EarningData_" + str(startDate[i]) + "_" + str(endDate[i]) + ".stock")

    dataBase = converter( filename ) 

#    outputFileName = "Symb_Date_Info_Nasdaq" + '/' +"EarningData_" + str(startDate[0]) + "_" + str(endDate[-1]) + ".stock"
    outputFileName = "Symb_Date_Info_Yahoo" + '/' +"EarningData_" + str(startDate[0]) + "_" + str(endDate[-1]) + ".stock"

    pickle.dump(dataBase, open(outputFileName,'wb'))

    res = pickle.load(open(outputFileName,'rb'))

    for symb in res.keys():
        print ' for the symbol = ' + str(symb)
        print res[symb]
        print ' --------------------------------'

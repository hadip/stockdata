import urllib2
from urllib2 import urlopen
import re
import cookielib
from cookielib import CookieJar
import time
import string
import datetime
import pickle

cj = CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
opener.addheaders = [('User-agent','Mozilla/5.0')]


# Alphabetic months
alphMonth = {1:'Jan', 2:'Feb', 3:'Mar', 4:'Apr', 5:'May', 6:'Jun', 7:'Jul', 8:'Aug', 9:'Sep', 10:'Oct', 11:'Nov', 12:'Dec'}
alphMonthRev = { v:k for k,v in alphMonth.items() }

# A funciton that checks if the string is a valid floating point number
def is_number(s):
    '''
    if re.match("^\d+?\.\d+?$", s) is None:
        return False
    else:
        return True
    '''
    try:
        float(s)
        return True
    except ValueError:
        return False

# decode characteristics of a company
def decodeFeatures( name, eTime ):
    '''
    This function converts the string corresponding to the features to numerical values
    '''

    # processing name
    if len(name) == 0: name_value = None
    else: name_value = name

    # processing eTime
    if len(eTime) == 0: eTime_value = None
    else: eTime_value = eTime

    return ( name_value, eTime_value )
    
        
    


def getEarning(date , depth ):

    if ( depth < 0 ): return None
    '''
    The input of the function is a date in (y,m,d) in integeres
    The output is a dict, with keys = symbols, and values are a tuple of its characteristics listed as follows:
    (company name, market cap, fiscal, estimated EPS, number of estimation, EPS)
    If any of the above values could not be extracted, we will put None in the tuple.
    If for the given date the function was not able to extract the data it returns None.
    '''
    # The earning date webpage from Nasdaq
    pageBase = 'http://biz.yahoo.com/research/earncal/'
    '''
    date need to be added to the link: 20141105.html
    '''
    try:
        # Assume the input is a tuple in intergers (year, month, day)
        year = date[0]
        month = date[1]
        day = date[2]
        # Creatung the date string from the tuple data
        dateString = str(year) + ("%02d" % month) + ("%02d" % day) 
        # Full link to the query date webpage
        page = pageBase+dateString+'.html'
        # Read the source file of the query page
        sourceCode = opener.open(page).read()

        ###############################################################################
        ########################### Earning dates scrapping ###########################
        ###############################################################################
        
        # Find all data for the companies for the given date

        contentWhite = re.findall(r'</td></tr><tr><td>[^@]*?</td></tr>',sourceCode)
        contentGrey = re.findall(r'<tr bgcolor=eeeeee><td>[^@]*?</td></tr><tr>',sourceCode)
            
        # we assume that a content which has a comany name also has the following substring:
        # href="http://www.nasdaq.com/earnings/report/
        keySubString = 'http://finance.yahoo.com/q?'
        # Index of contents with a company name
        compIndexWhite = [ i for i in range(len(contentWhite)) if string.find(contentWhite[i],keySubString) is not -1]
        compIndexGrey = [ i for i in range(len(contentGrey)) if string.find(contentGrey[i],keySubString) is not -1]
        # The output of the function
        outputDict ={}
        # Extract information for each company
        for i in compIndexWhite:
            # company name
            compName = re.findall(r'</td></tr><tr><td>.*</td><td>',contentWhite[i])[0][18:-9]
            # symbol
            symbol = re.findall(r'">.*?</a></td><td',contentWhite[i])[-1][2:-12]
            # time
            eTime = re.findall(r'<small>.*?</small>',contentWhite[i])[-1][7:-8]

            # if the symbol is not valid goto the next company
            if len(symbol) == 0: continue
            else:
                outputDict[symbol] = decodeFeatures( compName, eTime)

        for i in compIndexGrey:
            # company name
            compName = re.findall(r'<tr bgcolor=eeeeee><td>.*</td><td>',contentGrey[i])[0][23:-9]
            # symbol
            symbol = re.findall(r'">.*?</a></td><td',contentGrey[i])[-1][2:-12]
            # time
            eTime = re.findall(r'<small>.*?</small>',contentGrey[i])[-1][7:-8]

            # if the symbol is not valid goto the next company
            if len(symbol) == 0: continue
            else:
                outputDict[symbol] = decodeFeatures( compName, eTime)

        return outputDict
            
    except Exception, e:
        print e
        print ' Im a trying again with depth = ' + str(depth -1)
        return getEarning(date, depth-1 )

def get_historical_data( fromDate, days ):
    startDate = datetime.date(fromDate[0], fromDate[1], fromDate[2])
    dataBase = {}
    for i in range(days):
        date = startDate + datetime.timedelta(i)
        dateTuple = (date.year, date.month, date.day)
        dataBase[dateTuple] =  getEarning( dateTuple , 5 ) # Try 5 times :D
        print str(date) + " completed."
    folderName = "Date_Symb_Info_Yahoo"
    fileName = folderName + '/' + "EarningData_" + str(startDate) + "_" + str(date) + ".stock"
    pickle.dump(dataBase, open(fileName,'wb'))

if __name__ == '__main__':
    # print getEarning( (2013,6,4))
    fromDate = (2014,1,1)
    days = 304
    get_historical_data( fromDate , days )
    startDate = datetime.date(fromDate[0], fromDate[1], fromDate[2])
    date = startDate + datetime.timedelta( days-1 )
    folderName = "Date_Symb_Info_Yahoo"
    fileName = folderName + '/' +"EarningData_" + str(startDate) + "_" + str(date) + ".stock"
    res = pickle.load( open( fileName, 'rb') )
    for key in res.keys():
        print ' for the date = ' + str(key)
        print res[key]
        print ' --------------------------------'
